use cgmath::Vector2;
use components::{PositionComponent, UpdateMsg};

pub struct PlayerPositionComponent {
    pos: Vector2<f32>,
}

impl PlayerPositionComponent {
    pub fn new(pos: Vector2<f32>) -> PlayerPositionComponent {
        PlayerPositionComponent { pos: pos }
    }
}

impl PositionComponent for PlayerPositionComponent {
    fn x(&self) -> f32 {
        self.pos.x
    }

    fn y(&self) -> f32 {
        self.pos.y
    }

    fn vec(&self) -> Vector2<f32> {
        self.pos
    }

    fn update(&mut self, msg: UpdateMsg) {
        if let UpdateMsg::SetPosition(pos) = msg {
            self.pos = pos;
        }
    }
}

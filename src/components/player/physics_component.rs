use game_object::GameObject;
use components::{EventMsg, PositionComponent, PhysicsComponent, UpdateMsg};

const PLAYER_VELOCITY: f32 = 1000.0;

pub struct PlayerPhysicsComponent {}

impl PhysicsComponent for PlayerPhysicsComponent {
    fn update<Obj: GameObject>(&self,
                               obj: &Obj,
                               event: Option<EventMsg>,
                               elapsed: f32)
                               -> Option<UpdateMsg> {
        let mut position = obj.position().vec();
        let mut updated = false;
        let velocity = PLAYER_VELOCITY * elapsed;

        event.map(|event| {
            match event {
                EventMsg::KeyLeft => position.x -= velocity,
                EventMsg::KeyRight => position.x += velocity,
                EventMsg::KeyUp => position.y -= velocity,
                EventMsg::KeyDown => position.y += velocity,
            }
            updated = true;
        });

        // TODO(DarinM223): handle non-input related physics

        if updated {
            Some(UpdateMsg::SetPosition(position))
        } else {
            None
        }
    }
}

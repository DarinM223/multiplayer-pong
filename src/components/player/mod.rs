pub mod input_component;
pub mod physics_component;
pub mod position_component;

pub use self::input_component::PlayerInputComponent;
pub use self::physics_component::PlayerPhysicsComponent;
pub use self::position_component::PlayerPositionComponent;

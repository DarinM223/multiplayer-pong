use glium::glutin::{Event, ElementState, VirtualKeyCode};
use components::{EventMsg, InputComponent};

pub struct PlayerInputComponent {}

impl InputComponent for PlayerInputComponent {
    fn handle(&self, event: &Event) -> Option<EventMsg> {
        match *event {
            Event::KeyboardInput(ElementState::Pressed, _, Some(ref keycode)) => {
                match *keycode {
                    VirtualKeyCode::Left => Some(EventMsg::KeyLeft),
                    VirtualKeyCode::Right => Some(EventMsg::KeyRight),
                    VirtualKeyCode::Up => Some(EventMsg::KeyUp),
                    VirtualKeyCode::Down => Some(EventMsg::KeyDown),
                    _ => None,
                }
            }
            _ => None,
        }
    }
}

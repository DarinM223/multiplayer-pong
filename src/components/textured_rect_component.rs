use glium::{Surface, Frame, Program, VertexBuffer, IndexBuffer};
use glium::backend::Facade;
use glium::texture::Texture2d;
use glium::index::PrimitiveType;
use game_object::GameObject;
use components::{GraphicsComponent, PositionComponent, Vertex};


const VERTEX_SHADER: &'static str = r#"
    #version 140

    in vec2 position;

    uniform mat4 projection;

    out vec2 v_tex_coords;

    void main() {
        if (gl_VertexID % 4 == 0) { // First vertex
            v_tex_coords = vec2(0.0, 1.0);
        } else if (gl_VertexID % 4 == 1) { // Second vertex
            v_tex_coords = vec2(1.0, 1.0);
        } else if (gl_VertexID % 4 == 2) { // Third vertex
            v_tex_coords = vec2(0.0, 0.0);
        } else { // Fourth vertex
            v_tex_coords = vec2(1.0, 0.0);
        }

        gl_Position = projection * vec4(position, 0.0, 1.0);
    }
"#;

const FRAGMENT_SHADER: &'static str = r#"
    #version 140

    in vec2 v_tex_coords;

    out vec4 color;

    uniform sampler2D tex;

    void main() {
        color = texture(tex, v_tex_coords);
    }
"#;

pub struct TexturedRectGraphicsComponent {
    width: f32,
    height: f32,
    program: Program,
    perspective: [[f32; 4]; 4],
    texture: Texture2d,
    vertices: VertexBuffer<Vertex>,
    indices: IndexBuffer<u16>,
}

impl TexturedRectGraphicsComponent {
    pub fn new<F: Facade>(width: f32,
                          height: f32,
                          display: &F,
                          texture: Texture2d,
                          perspective: [[f32; 4]; 4])
                          -> TexturedRectGraphicsComponent {
        let ib_data: Vec<u16> = vec![0, 1, 2, 1, 3, 2];

        let vb = VertexBuffer::empty_dynamic(display, 4).unwrap();
        let ib = IndexBuffer::new(display, PrimitiveType::TrianglesList, &ib_data).unwrap();
        let program = Program::from_source(display, VERTEX_SHADER, FRAGMENT_SHADER, None).unwrap();

        TexturedRectGraphicsComponent {
            width: width,
            height: height,
            program: program,
            perspective: perspective,
            texture: texture,
            vertices: vb,
            indices: ib,
        }
    }
}

impl GraphicsComponent for TexturedRectGraphicsComponent {
    fn render<Obj: GameObject>(&self, obj: &Obj, frame: &mut Frame) {
        {
            let position = obj.position().vec();
            let left = position.x - self.width / 2.0;
            let right = position.x + self.width / 2.0;
            let bottom = position.y + self.height / 2.0;
            let top = position.y - self.height / 2.0;
            let vb_data = vec![Vertex { position: [left, top] },
                               Vertex { position: [right, top] },
                               Vertex { position: [left, bottom] },
                               Vertex { position: [right, bottom] }];
            self.vertices.write(&vb_data);
        }

        {
            // Uniform parameters to pass into the shaders.
            let uniforms = uniform! {
                projection: self.perspective,
                tex: &self.texture,
            };

            frame.draw(&self.vertices,
                      &self.indices,
                      &self.program,
                      &uniforms,
                      &Default::default())
                .unwrap();
        }
    }
}

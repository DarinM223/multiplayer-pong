pub mod player;
pub mod textured_rect_component;

use glium::Frame;
use glium::glutin::Event;
use game_object::GameObject;
use cgmath::Vector2;

#[derive(Clone, Copy)]
struct Vertex {
    position: [f32; 2],
}

implement_vertex!(Vertex, position);

pub enum UpdateMsg {
    SetPosition(Vector2<f32>),
    SetVelocity(Vector2<f32>),
}

pub enum EventMsg {
    KeyLeft,
    KeyRight,
    KeyUp,
    KeyDown,
}

pub trait PositionComponent {
    fn x(&self) -> f32;
    fn y(&self) -> f32;
    fn vec(&self) -> Vector2<f32>;
    fn update(&mut self, msg: UpdateMsg);
}

pub trait InputComponent {
    fn handle(&self, event: &Event) -> Option<EventMsg>;
}


pub trait PhysicsComponent {
    fn update<Obj: GameObject>(&self,
                               obj: &Obj,
                               event: Option<EventMsg>,
                               elapsed: f32)
                               -> Option<UpdateMsg>;
}

pub trait GraphicsComponent {
    fn render<Obj: GameObject>(&self, obj: &Obj, frame: &mut Frame);
}

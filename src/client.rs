#[macro_use]
extern crate glium;
extern crate cgmath;
extern crate image;

pub mod components;
pub mod game_object;

use components::{PositionComponent, GraphicsComponent, InputComponent, PhysicsComponent};
use components::textured_rect_component::TexturedRectGraphicsComponent;
use components::player::{PlayerPositionComponent, PlayerInputComponent, PlayerPhysicsComponent};
use game_object::{GameObject, GameObjectImpl};

use cgmath::{Vector2, Matrix4};
use glium::glutin;
use glium::{DisplayBuild, Surface};
use std::io::Cursor;
use std::time::Instant;

const SCREEN_WIDTH: u32 = 1024;
const SCREEN_HEIGHT: u32 = 768;

fn main() {
    let display = glutin::WindowBuilder::new()
        .with_dimensions(SCREEN_WIDTH, SCREEN_HEIGHT)
        .with_title(format!("Hello world!"))
        .build_glium()
        .unwrap();

    // Load the texture.
    let texture = {
        let img = image::load(Cursor::new(&include_bytes!("../assets/megumin.png")[..]),
                              image::PNG)
            .unwrap()
            .to_rgba();

        let img_dim = img.dimensions();
        let img = glium::texture::RawImage2d::from_raw_rgba_reversed(img.into_raw(), img_dim);

        glium::texture::Texture2d::new(&display, img).unwrap()
    };

    let perspective = {
        let matrix: Matrix4<f32> = cgmath::ortho(0.0,
                                                 SCREEN_WIDTH as f32,
                                                 SCREEN_HEIGHT as f32,
                                                 0.0,
                                                 -1.0,
                                                 1.0);
        Into::<[[f32; 4]; 4]>::into(matrix)
    };

    let rect_position = Vector2 {
        x: (SCREEN_WIDTH / 2) as f32,
        y: (SCREEN_HEIGHT / 2) as f32,
    };

    let mut rect = GameObjectImpl::new(PlayerPositionComponent::new(rect_position),
                                       PlayerInputComponent {},
                                       PlayerPhysicsComponent {},
                                       TexturedRectGraphicsComponent::new(300.,
                                                                          300.,
                                                                          &display,
                                                                          texture,
                                                                          perspective));

    let mut last_frame = Instant::now();
    let mut elapsed;
    let mut event_msg = None;

    // Main event loop where all the drawing code is contained.
    loop {
        {
            let dt = last_frame.elapsed().subsec_nanos() as f32 / 1.0e6;
            elapsed = dt / 1.0e3;
            last_frame = Instant::now();
        }

        let mut frame = display.draw();

        // Start with white background.
        frame.clear_color(1.0, 1.0, 1.0, 1.0);

        rect.graphics().render(&rect, &mut frame);

        frame.finish().unwrap();

        // Handles keyboard input.
        for event in display.poll_events() {
            event_msg = rect.input().handle(&event);

            match event {
                glutin::Event::Closed => return,
                glutin::Event::KeyboardInput(glutin::ElementState::Pressed,
                                             _,
                                             Some(glutin::VirtualKeyCode::Escape)) => return,
                _ => {}
            }
        }

        rect.physics()
            .update(&rect, event_msg.take(), elapsed)
            .map(|update_msg| rect.position_update(|pos| pos.update(update_msg)));
    }
}

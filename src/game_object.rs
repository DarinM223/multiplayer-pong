use components::{PositionComponent, InputComponent, PhysicsComponent, GraphicsComponent};

pub trait GameObject {
    type Position: PositionComponent;
    type Input: InputComponent;
    type Physics: PhysicsComponent;
    type Graphics: GraphicsComponent;

    fn position<'a>(&'a self) -> &'a Self::Position;
    fn input<'a>(&'a self) -> &'a Self::Input;
    fn physics<'a>(&'a self) -> &'a Self::Physics;
    fn graphics<'a>(&'a self) -> &'a Self::Graphics;

    fn position_update<F>(&mut self, f: F) where F: FnOnce(&mut Self::Position);
}

pub struct GameObjectImpl<Pos, In, Phy, Gr>
    where Pos: PositionComponent,
          In: InputComponent,
          Phy: PhysicsComponent,
          Gr: GraphicsComponent
{
    pos: Pos,
    inp: In,
    phy: Phy,
    gra: Gr,
}

impl<Pos, In, Phy, Gr> GameObjectImpl<Pos, In, Phy, Gr>
    where Pos: PositionComponent,
          In: InputComponent,
          Phy: PhysicsComponent,
          Gr: GraphicsComponent
{
    pub fn new(position_component: Pos,
               input_component: In,
               physics_component: Phy,
               graphics_component: Gr)
               -> GameObjectImpl<Pos, In, Phy, Gr> {
        GameObjectImpl {
            pos: position_component,
            inp: input_component,
            phy: physics_component,
            gra: graphics_component,
        }
    }
}

impl<Pos, In, Phy, Gr> GameObject for GameObjectImpl<Pos, In, Phy, Gr>
    where Pos: PositionComponent,
          In: InputComponent,
          Phy: PhysicsComponent,
          Gr: GraphicsComponent
{
    type Position = Pos;
    type Input = In;
    type Physics = Phy;
    type Graphics = Gr;

    fn position<'a>(&'a self) -> &'a Self::Position {
        &self.pos
    }

    fn input<'a>(&'a self) -> &'a Self::Input {
        &self.inp
    }

    fn physics<'a>(&'a self) -> &'a Self::Physics {
        &self.phy
    }

    fn graphics<'a>(&'a self) -> &'a Self::Graphics {
        &self.gra
    }

    fn position_update<F>(&mut self, f: F)
        where F: FnOnce(&mut Self::Position)
    {
        f(&mut self.pos);
    }
}
